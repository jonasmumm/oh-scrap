﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GGJ20/Car/CarControllerBehaviour")]
public class CarControllerBehaviourDef : ScriptableObject
{
    [SerializeField] float forceMin;
    [SerializeField] float forceMax;
    [SerializeField] float forceTurn;
    [SerializeField] float rotationAmount; //visual only
    [SerializeField] float rotationForce;

    [SerializeField] Vector2 noiseAmount;
    [SerializeField] Vector2 noiseSpeed;

    public float ForceMin => forceMin;
    public float ForceMax => forceMax;
    public float ForceTurn => forceTurn;
    public float RotationAmount => rotationAmount;
    public float Rotationforce => rotationForce;

    public Vector2 NoiseAmount => noiseAmount;
    public Vector2 NoiseSpeed => noiseSpeed;
}