﻿using UnityEngine;

public class TitleMenu : MonoBehaviour
{
    [SerializeField] ChaseManager chaseManager;

    public void StartGame()
    {
        chaseManager.InitChase(1);
        gameObject.SetActive(false);
    }
}
