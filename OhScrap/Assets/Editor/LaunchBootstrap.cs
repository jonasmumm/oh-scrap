﻿using UnityEditor;
using UnityEditor.SceneManagement;

[InitializeOnLoad]
public static class SimpleEditorUtils
{
    [MenuItem("Tools/Play from Bootstrap #&O")]
    public static void PlayFromPrelaunchScene()
    {
        if (EditorApplication.isPlaying == true)
        {
            EditorApplication.isPlaying = false;
            return;
        }

        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/Scenes/Bootstrap.unity");

        EditorApplication.isPlaying = true;
    }
}