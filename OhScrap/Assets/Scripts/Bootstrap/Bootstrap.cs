﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bootstrap : MonoBehaviour
{
    [SerializeField] string nextSceneName;

    private void Awake()
    {
        SceneManager.LoadScene(nextSceneName, LoadSceneMode.Additive);
    }
}
