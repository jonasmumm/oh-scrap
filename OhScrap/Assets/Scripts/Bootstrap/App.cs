﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RSG;
using System;
using UnityEngine.SceneManagement;

public class App : MonoBehaviour
{
    public static PromiseTimer PromiseTimer;
    private static bool _restart;

    private void Awake()
    {
        PromiseTimer = new PromiseTimer();
    }

    private void Update()
    {
        PromiseTimer.Update(Time.deltaTime);
    }

    internal static void RestartEveything()
    {
        if (!_restart)
        {
            _restart = true;
            PromiseTimer.WaitFor(0.6f).Then(() =>
            {
                SceneManager.UnloadSceneAsync("Convoy");

                return PromiseTimer.WaitFor(0.6f);
            }).Then(() =>
            {
                SceneManager.LoadScene("Convoy", LoadSceneMode.Additive);
                return PromiseTimer.WaitFor(0.5f);
            }).Then(() =>
            {
                _restart = false;
            }).Done();
        }

    }

    private static Action<AsyncOperation> LoadIt()
    {


        return null;
    }
}
