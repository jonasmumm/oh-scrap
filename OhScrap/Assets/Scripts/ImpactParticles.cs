﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactParticles : MonoBehaviour
{
    [SerializeField] ParticleSystem part;

    private void Start()
    {
        part.Stop();

        App.PromiseTimer.WaitFor(3f).Then(() =>
        {
            Destroy(this.gameObject);
        }).Done();
    }
}
