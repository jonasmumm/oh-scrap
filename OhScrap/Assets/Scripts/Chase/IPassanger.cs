﻿using UnityEngine;

public interface IPassanger
{
    Rigidbody GetRigidbody();
}
