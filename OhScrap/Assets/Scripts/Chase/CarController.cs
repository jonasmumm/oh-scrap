﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class CarController : OutOfThisWorldListener
{
    const float spawnedObjectFreezeTime = 0.5f;

    [SerializeField] Rigidbody rigidBodo;
    [SerializeField] CarControllerBehaviourDef behaviour;
    [SerializeField] AnimationCurve passengerEjectAnimationCurve;
    [SerializeField] Transform[] spawnPoints;
    [SerializeField] AudioSource audioData;
    [SerializeField] float carHealth;
    [SerializeField] ParticleSystem impactEffectPrefab;
    [SerializeField] Transform destroyedPrefab;

    CarController _target;
    Vector3 _targetOffset;

    Vector2 _noiseSamplePositionX;
    Vector2 _noiseSamplePositionY;
    List<Tuple<BridgeableItem, BridgeableItem>> _connections = new List<Tuple<BridgeableItem, BridgeableItem>>();
    public CarController other;
    private float _zSpeed;
    private IPlan _plan;
    private List<IPassanger> _passengers = new List<IPassanger>();

    private void Awake()
    {
        _noiseSamplePositionX = new Vector3(Random.Range(-127999f, 127999f), 0f, Random.Range(-127999f, 127999f));
        _noiseSamplePositionY = new Vector3(Random.Range(-127999f, 127999f), 0f, Random.Range(-127999f, 127999f));
    }

    private void OnDestroy()
    {
        DisconnectAll();
        var instance = Instantiate(destroyedPrefab, transform.position, transform.rotation);
        Destroy(instance.gameObject, 3f);
    }

    public void SetVirtualVelocity(float zSpeed)
    {
        _zSpeed = zSpeed;
    }

    private void OnCollisionEnter(Collision collision)
    {
        var force = collision.impulse;

        var pointOfImpact = collision.GetContact(0);

        var particleSys = Instantiate(impactEffectPrefab, pointOfImpact.point, Quaternion.Euler(180f, 0f, 0f), transform);

        carHealth -= force.magnitude;

        if (carHealth <= 0)
        {
            Destroy(gameObject);
            audioData.Play(0);
        }
    }

    public void SpawnPassenger(IPassanger passenger, int posIndex)
    {
        var spawn = spawnPoints[posIndex % spawnPoints.Length];

        passenger.GetRigidbody().isKinematic = true;
        App.PromiseTimer.WaitUntil((td) =>
        {
            passenger.GetRigidbody().transform.position = spawn.position;
            passenger.GetRigidbody().velocity = rigidBodo.velocity;

            return td.elapsedTime >= spawnedObjectFreezeTime;
        }).Then(() =>
        {
            passenger.GetRigidbody().isKinematic = false;
        }).Done();

        _passengers.Add(passenger);
    }

    public void ApplyBehaviour(CarControllerBehaviourDef behaviour)
    {
        this.behaviour = behaviour;
    }


    public void StartPlan(IPlan plan)
    {
        _plan = plan;
    }

    public void SetTarget(CarController target, Vector3? offset = null)
    {
        _target = target;

        if (offset.HasValue) _targetOffset = offset.Value;
    }

    protected override void Update()
    {
        base.Update();

        if (_plan != null)
        {
            _plan.UpdatePlan();
        }
    }

    private void FixedUpdate()
    {
        if (_plan != null)
        {
            _noiseSamplePositionX += Time.fixedDeltaTime * behaviour.NoiseSpeed;
            _noiseSamplePositionY += Time.fixedDeltaTime * behaviour.NoiseSpeed;

            var targetOffsetWithNoise = _targetOffset +
                 new Vector3((Mathf.PerlinNoise(_noiseSamplePositionX.x, _noiseSamplePositionX.y) - 0.5f) * behaviour.NoiseAmount.x, 0f,
                 (Mathf.PerlinNoise(_noiseSamplePositionY.x, _noiseSamplePositionY.y) - 0.5f) * behaviour.NoiseAmount.y);

            var zTarget = _target != null
                ? _target.transform.position.z + targetOffsetWithNoise.z
                : targetOffsetWithNoise.z;

            var straightforce = (gameObject.transform.position.z > zTarget)
            ? behaviour.ForceMin
            : behaviour.ForceMax;

            var xTarget = _target != null
                ? _target.transform.position.x + targetOffsetWithNoise.x
                : targetOffsetWithNoise.x;

            var sideforce = (gameObject.transform.position.x > xTarget)
                    ? -behaviour.ForceTurn
                    : behaviour.ForceTurn;

            var desiredAngle = Vector2.SignedAngle(new Vector2(0, 1f), new Vector2(rigidBodo.velocity.x, rigidBodo.velocity.z + _zSpeed)) * behaviour.RotationAmount;

            var torque = Mathf.DeltaAngle(transform.rotation.y * Mathf.Rad2Deg, -desiredAngle);

            rigidBodo.AddForce(sideforce, 0, straightforce, ForceMode.Force);
            rigidBodo.AddTorque(0f, torque * behaviour.Rotationforce, 0f);
        }
    }

    public void ConnectOther(BridgeableItem we, BridgeableItem other)
    {
        if (_connections.Contains(new Tuple<BridgeableItem, BridgeableItem>(we, other)) || _connections.Contains(new Tuple<BridgeableItem, BridgeableItem>(other, we))) return;
        var carIsAlreadyConnected = _connections.Any(x => x.Item2 == other);

        _connections.Add(new Tuple<BridgeableItem, BridgeableItem>(we, other));
        other.CarController._connections.Add(new Tuple<BridgeableItem, BridgeableItem>(other, we));

        if (!carIsAlreadyConnected)
        {
            var otherWorldPos = other.transform.position;

            other.CarController.transform.SetParent(transform, true);
            other.CarController.rigidBodo.isKinematic = true;
            other.CarController.rigidBodo.velocity = Vector3.zero;
            other.CarController.rigidBodo.angularVelocity = Vector3.zero;
            other.CarController.CancelPlan();
            other.enabled = false;
        }
    }

    public void DisconnectAll()
    {
        for (var i = _connections.Count - 1; i >= 0; i--)
        {
            var connection = _connections[i];
            _connections.Remove(connection);

            var other = connection.Item1.CarController;

            connection.Item1.DissolveBridge();
            connection.Item2.DissolveBridge();

            var carIsStillConnected = _connections.Any(x => x.Item2.CarController == other);

            if (!carIsStillConnected && other != null)
            {
                other.transform.SetParent(transform.parent);

                if (transform.parent.GetComponent<CarController>() == null)
                {
                    other.rigidBodo.isKinematic = false;
                    other.StartPlan(new PullUpAndRam(other, GameObject.FindObjectsOfType<CarController>().FirstOrDefault()));
                    other.enabled = true;
                }
            }
        }
    }

    void CancelPlan()
    {
        if (_plan != null)
        {
            _plan.Cancel();
            _plan = null;
        }
    }

    public CarController FindClosestConnected(Vector3 pos)
    {
        List<CarController> beenThere = new List<CarController>();
        var currentClosest = this;
        var currentClosestDist = (transform.position - pos).sqrMagnitude;

        return DoTheRecursiveClosestStuff(this);

        CarController DoTheRecursiveClosestStuff(CarController checkingAgainst)
        {
            beenThere.Add(checkingAgainst);
            var otherVehicles = checkingAgainst._connections.Select(t => t.Item2.CarController).Distinct();
            var dist = (checkingAgainst.transform.position - pos).sqrMagnitude;

            if (dist < currentClosestDist)
            {
                currentClosestDist = dist;
                currentClosest = checkingAgainst;
            }

            foreach (var v in otherVehicles)
            {
                if (!beenThere.Contains(v))
                {
                    DoTheRecursiveClosestStuff(v);
                }
            }
            return currentClosest;
        }
    }

    public interface IPlan
    {
        void Cancel();
        void UpdatePlan();
    }

    public class JustCruising : IPlan
    {
        public void Cancel()
        {
        }

        public void UpdatePlan()
        {
            //:)
        }
    }

    public class PullUpAndRam : IPlan
    {
        enum State
        {
            PullUp,
            Lurk,
            Ram,
            FallBack
        }

        CarController _me;
        CarController _targetMain;
        CarController _target;
        State _state;

        public PullUpAndRam(CarController me, CarController target)
        {
            _me = me;
            _targetMain = target;
            var angleOfAttack = Random.Range(45f, 135f) * Mathf.Deg2Rad;
            var attackOffset = new Vector3(Mathf.Sin(angleOfAttack), 0f, Mathf.Cos(angleOfAttack)) * 1000f;
            if (Random.Range(0, 2) == 0) attackOffset.x = attackOffset.x * -1;

            _target = _targetMain.FindClosestConnected(_targetMain.transform.position + attackOffset);
            _state = State.PullUp;

            me._target = _target;
            me._targetOffset = new Vector3(5f * (attackOffset.x > 0f ? 1f : -1f), 0f, 0f);
        }

        public void UpdatePlan()
        {
            if (!_target) _target = ChaseManager.FindACar();

            switch (_state)
            {
                case State.PullUp:

                    if (Mathf.Abs(_me.transform.position.z - _target.transform.position.z) < 2f)
                    {
                        StartLurke();
                    }
                    break;

                case State.FallBack:
                    if (_me.transform.position.z < _targetMain.transform.position.z - 50f)
                    {
                        Destroy(_me.gameObject);
                    }
                    break;
            }
        }

        private void StartLurke()
        {
            _state = State.Lurk;
            _me._targetOffset = new Vector3(5f * Mathf.Sign(_me.transform.position.x - _target.transform.position.x), 0f, 0f);

            App.PromiseTimer.WaitFor(Random.Range(0.5f, 5f)).Then(() =>
            {
                _state = State.Ram;
                _me._targetOffset = Vector3.zero;
            }).Then(() => App.PromiseTimer.WaitFor(Random.Range(2f, 4.2f))).Then(() =>
              {
                  if (Random.Range(0, 5) == 0)
                  {
                      _state = State.FallBack;
                      _me._targetOffset.z = -100;
                  }
                  else
                  {
                      StartLetoff();
                  }
              }).Done();
        }

        private void StartLetoff()
        {
            _state = State.Lurk;
            _me._targetOffset = new Vector3(5f * Mathf.Sign(_me.transform.position.x - _target.transform.position.x), 0f, 5f * Mathf.Sign(_me.transform.position.z - _target.transform.position.z));

            App.PromiseTimer.WaitFor(Random.Range(4f, 5f)).Then(() => { StartLurke(); }).Done();
        }

        public void Cancel()
        {
            _me._target = null;
            _me._targetOffset = Vector3.zero;
        }
    }

    public class DeployTheTroops : IPlan
    {
        enum State
        {
            Approaching,
            Deploying
        }

        CarController _me;
        CarController _targetMain;
        CarController _target;
        State _state;

        public DeployTheTroops(CarController me, CarController target)
        {
            _state = State.Approaching;
            _me = me;
            _targetMain = target;
            var angleOfAttack = Random.Range(45f, 135f) * Mathf.Deg2Rad;
            var attackOffset = new Vector3(Mathf.Sin(angleOfAttack), 0f, Mathf.Cos(angleOfAttack)) * 1000f;
            if (Random.Range(0, 2) == 0) attackOffset.x = attackOffset.x * -1;

            _target = _targetMain.FindClosestConnected(_targetMain.transform.position + attackOffset);

            me._target = _target;
            me._targetOffset = new Vector3(5f * (attackOffset.x > 0f ? 1f : -1f), 0f, 0f);
        }

        public void UpdatePlan()
        {
            if (!_target) _target = ChaseManager.FindACar();

            switch (_state)
            {
                case State.Approaching:

                    if (Mathf.Abs(_me.transform.position.z - _target.transform.position.z) < 2f)
                    {
                        _state = State.Deploying;

                        App.PromiseTimer.WaitFor(1f).Then(() =>
                        {
                            if (_me._passengers.Count > 0)
                            {
                                PassengerEject(_me._passengers[0], 0, _me, _target);
                            }
                            else
                            {
                                _me.StartPlan(new PullUpAndRam(_me, _targetMain));
                            }
                        }).Done();
                    }
                    break;
            }
        }

        private void PassengerEject(IPassanger passanger, int index, CarController me, CarController target)
        {
            float jumpTime = 3f;
            float jumpHeigth = 5f;

            var startPos = passanger.GetRigidbody().transform.position;
            var endPos = target.spawnPoints[index];

            passanger.GetRigidbody().isKinematic = true;
            passanger.GetRigidbody().velocity = Vector3.zero;
            passanger.GetRigidbody().angularVelocity = Vector3.zero;
            passanger.GetRigidbody().transform.rotation = Quaternion.identity;

            App.PromiseTimer.WaitUntil((td) =>
             {
                 var progress = td.elapsedTime / jumpTime;

                 var height = _me.passengerEjectAnimationCurve.Evaluate(progress) * jumpHeigth;
                 var position = Vector3.Lerp(startPos, endPos.position, progress) + new Vector3(0f, height, 0f);

                 passanger.GetRigidbody().gameObject.transform.position = position;

                 return progress >= 1f;
             }).Then(() =>
             {
                 passanger.GetRigidbody().isKinematic = false;
             });

        }

        public void Cancel()
        {
            _me._target = null;
            _me._targetOffset = Vector3.zero;
        }
    }
}
