﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using static CarController;
using Random = UnityEngine.Random;

public class ChaseManager : MonoBehaviour
{
    [SerializeField] WorldScroller worldScroller;
    [SerializeField] WorldScroller colliderScroller;
    [SerializeField] Cinemachine.CinemachineVirtualCamera cam;
    [SerializeField] Cinemachine.CinemachineTargetGroup camTargetGroup;
    [SerializeField] Transform[] players;
    [SerializeField] float killZoneOffset;
    [SerializeField] CarController[] carPrefabs;
    [SerializeField] PlayerController playerPrefab;
    [SerializeField] CarController startCar;
    [SerializeField] float zSpeed;

    [SerializeField] float spawnRate;
    [SerializeField] float spawnRateModifierPerSpawn;
    [SerializeField] float spawnRateMin = 1.5f;

    [SerializeField] PoliceController misterStoopid;

    Vector3 _chaseCenter;
    float _killZoneZ;

    Coroutine _spawning;
    private static ChaseManager _instance;

    private void Awake()
    {
        _instance = this;
        _chaseCenter = Vector3.zero;
        startCar.SetVirtualVelocity(zSpeed);
        startCar.StartPlan(new JustCruising());
        worldScroller.SetSpeed(zSpeed);
        colliderScroller.SetSpeed(zSpeed);
    }

    private void OnDestroy()
    {
        StopCoroutine(_spawning);
    }

    private IEnumerator SpawnContinously()
    {
        yield return new WaitForSeconds(spawnRate);

        var c = SpawnCar();
        c.StartPlan(new DeployTheTroops(c, FindACar()));

        for (var j = 0; j < Random.Range(0, 5); j++)
        {
            var policeBoy = Instantiate(misterStoopid, transform);
            c.SpawnPassenger(policeBoy, j);
        }

        spawnRate = Mathf.Max(spawnRateMin, spawnRate * spawnRateModifierPerSpawn);

        _spawning = StartCoroutine(SpawnContinously());
    }

    public static CarController FindACar()
    {
        var car = _instance.startCar;

        if (car == null)
        {
            car = FindObjectsOfType<CarController>().FirstOrDefault();
        }

        if (car == null)
        {
            App.RestartEveything();
        }

        return car;
    }

    public void InitChase(int players)
    {
        for (var i = 0; i < players; i++)
        {
            var player = Instantiate(playerPrefab, transform);
            startCar.SpawnPassenger(player, i);
            camTargetGroup.AddMember(player.transform, 1, 4f);
        }

        var cars = new CarController[2];
        for (var i = 0; i < cars.Length; i++)
        {
            cars[i] = SpawnCar();
            cars[i].StartPlan(new DeployTheTroops(cars[i], startCar));

            for (var j = 0; j < 4; j++)
            {
                var policeBoy = Instantiate(misterStoopid, transform);
                cars[i].SpawnPassenger(policeBoy, j);
            }

        }

        _spawning = StartCoroutine(SpawnContinously());
    }

    private void Update()
    {
        _chaseCenter = Vector3.zero;
        foreach (var p in players)
        {
            _chaseCenter += p.position / players.Length;
        }

        foreach (var p in players)
        {
            if (p.transform.position.z < _chaseCenter.z - killZoneOffset)
            {
                //TODO: Kill ? Or die on impact with road?
            }
        }

        //worldScroller.SetZMinPos(cam.transform.position.z);
        //colliderScroller.SetZMinPos(cam.transform.position.z);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(_chaseCenter, 3);
        Gizmos.DrawCube(new Vector3(0, 0, _chaseCenter.z - killZoneOffset), new Vector3(100, 2, 0.1f));
    }

    public CarController SpawnCar()
    {
        var car = Instantiate(carPrefabs[Random.Range(0, carPrefabs.Length)], transform);

        var defaultCar = FindACar();

        car.transform.position = defaultCar.transform.position + new Vector3(Random.Range(-20f, 20f), 1f, 50);
        car.SetVirtualVelocity(zSpeed);
        car.SetTarget(startCar, Vector3.zero);

        return car;
    }
}
