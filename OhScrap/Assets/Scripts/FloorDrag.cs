﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorDrag : MonoBehaviour
{
    [SerializeField] Rigidbody rb;
    [SerializeField] float drag;

    Rigidbody _dragRigidbody;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.rigidbody && collision.rigidbody.GetComponent<FloorDragGiver>())
        {
            _dragRigidbody = collision.gameObject.GetComponent<Rigidbody>();
        }
    }

    private void FixedUpdate()
    {
        if(!_dragRigidbody)
        {
            return;
        }
        var newVelocity = Damp(rb.velocity, _dragRigidbody.velocity, drag, Time.fixedDeltaTime);

        rb.AddForce(newVelocity - rb.velocity, ForceMode.VelocityChange);  
    }

    public Vector3 Damp(Vector3 a, Vector3 b, float lambda, float dt)
    {
        return Vector3.Lerp(a, b, 1 - Mathf.Exp(-lambda * dt));
    }

}
