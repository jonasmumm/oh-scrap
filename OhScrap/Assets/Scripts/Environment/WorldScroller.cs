﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldScroller : MonoBehaviour
{
    [SerializeField] WorldTile[] prefabs;
    [SerializeField] float prefabLength;
    [SerializeField] float prefabPreOffset;
    [SerializeField] float prefabPostOffset;
    [SerializeField] Vector3 prefabRotation;

    WorldTile[] _children;
    int _childrenNeeded;
    float _zPosition;
    int _currentLastChildIndex;
    int _segmentsSkipped;
    private float _zSpeed;

    void Awake()
    {
        InitRoad();
        _zSpeed = 20;
        _currentLastChildIndex = 0;
    }

    private void Update()
    {
        transform.position = transform.transform.position - new Vector3(0f, 0f, _zSpeed)*Time.deltaTime;
        while (transform.transform.position.z < (_segmentsSkipped + 1) * -prefabLength)
        {
            _segmentsSkipped += 1;
            _children[_currentLastChildIndex].transform.localPosition = new Vector3(0, 0, ((_segmentsSkipped - 1) + _childrenNeeded) * prefabLength - prefabPostOffset);
            _children[_currentLastChildIndex].OnResetPosition();
            _currentLastChildIndex = (_currentLastChildIndex + 1) % _childrenNeeded;
        }
    }

    public void SetSpeed(float zSpeed)
    {
        _zSpeed = zSpeed;

    }

    void InitRoad()
    {
        for (var i = 0; i < transform.childCount; i++)
        {
            var c = transform.GetChild(i);
            Destroy(c.gameObject);
        }

        _childrenNeeded = (int)(((prefabPreOffset+prefabPostOffset) / prefabLength) + 1);

        _children = new WorldTile[_childrenNeeded];

        for (var i = 0; i < _children.Length; i++)
        {
            _children[i] = Instantiate(prefabs[Random.Range(0, prefabs.Length)], new Vector3(0, 0, -prefabPostOffset+(i) * prefabLength), Quaternion.Euler(prefabRotation), transform);
        }

        _currentLastChildIndex = 0;
    }

}
