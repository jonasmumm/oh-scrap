﻿using UnityEngine;

public class DestructibleEnvironmentItem : MonoBehaviour
{
    public GameObject destroyedObject;
    GameObject _destroyedInstance;
    [SerializeField] AudioSource audioData;


    private void OnCollisionEnter(Collision collision)
    {
        audioData.Play(0);
        _destroyedInstance = Instantiate(destroyedObject, gameObject.transform.position, transform.rotation, gameObject.transform.parent);
        gameObject.SetActive(false);
    }

    public void ResetItem()
    {
        if (_destroyedInstance) Destroy(_destroyedInstance);
        gameObject.SetActive(true);
    }

}
