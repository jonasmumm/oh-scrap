﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollStreet : WorldTile
{
    [SerializeField, Range(0, 1)] float friction;
    [SerializeField] Vector3 speed;

    List<Rigidbody> rb = new List<Rigidbody>();

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.rigidbody != null && collision.rigidbody.tag != "Car")
        {
            rb.Add(collision.rigidbody);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.rigidbody != null && rb.Contains(collision.rigidbody))
        {
            rb.Remove(collision.rigidbody);
        }
    }

    private void Update()
    {
        for  (var i=rb.Count-1; i>=0; i--)
        {
            var r = rb[i];
            if (r)
            {
                r.AddForce(Damp(r.velocity, speed, friction, Time.deltaTime) - r.velocity, ForceMode.VelocityChange);
            }
            else
            {
                rb.Remove(r);
            }
        }
    }

    public Vector3 Damp(Vector3 a, Vector3 b, float lambda, float dt)
    {
        return Vector3.Lerp(a, b, 1 - Mathf.Exp(-lambda * dt));
    }
}
