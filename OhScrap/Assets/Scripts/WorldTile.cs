﻿using UnityEngine;

public class WorldTile : MonoBehaviour
{
    [SerializeField] DestructibleEnvironmentItem[] enviroItems;

    public void OnResetPosition()
    {
        foreach(var e in enviroItems)
        {
            e.ResetItem();
        }
    }
}
