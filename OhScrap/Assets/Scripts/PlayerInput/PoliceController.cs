﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceController : OutOfThisWorldListener, IPassanger
{
    const float PoliceRadius = 5;

    [SerializeField] Animator animator;
    [SerializeField] Rigidbody rb;
    [SerializeField] float speed;

    Transform currentEnemy;
    bool _startedInteracting;

    private void Start()
    {
        currentEnemy = GetClosestItemOrPlayer(FindAllPlayersAndItems());
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        currentEnemy = GetClosestItemOrPlayer(FindAllPlayersAndItems());
        if (currentEnemy == null) return;

        MoveTo(currentEnemy);

        if (currentEnemy.TryGetComponent(out RepairableItem item) && _startedInteracting)
        {
            if (item.State == ItemState.NotBroken)
            {
                item.PoliceInteraction();
            }
            else
            {
                animator.SetBool("IsInteracting", false);
            }
        }
    }

    Transform[] FindAllPlayersAndItems()
    {
        var possiblePlayersItems = Physics.OverlapSphere(transform.position, PoliceRadius);
        var result = new Transform[possiblePlayersItems.Length];

        for (var i = 0; i < result.Length; i++)
        {
            var c = possiblePlayersItems[i];

            if (c.TryGetComponent(out PlayerController player))
            {
                result[i] = player.transform;
            }
            if(c.TryGetComponent(out RepairableItem item))
            {
                if (item.State == ItemState.Broken) continue;
                result[i] = item.transform;
            }
        }
        return result;
    }

    // https://forum.unity.com/threads/clean-est-way-to-find-nearest-object-of-many-c.44315/ :-)
    Transform GetClosestItemOrPlayer(Transform[] playersItems)
    {
        Transform closestPlayerItem = null;

        float closestDistance = Mathf.Infinity;
        var currentPos = transform.position;

        foreach (Transform potentialTarget in playersItems)
        {
            if (potentialTarget == null || potentialTarget.gameObject == gameObject) continue;
            Vector3 directionToTarget = potentialTarget.position - currentPos;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistance)
            {
                if(closestPlayerItem != null && closestPlayerItem.TryGetComponent(out RepairableItem item))
                {
                    if (item.State == ItemState.Broken) continue;
                }
                closestDistance = dSqrToTarget;
                closestPlayerItem = potentialTarget;
            }
        }

        return closestPlayerItem;
    }

    void MoveTo(Transform playerItem)
    {
        transform.LookAt(playerItem);
        //var direction = new Vector3(playerItem.position.x, transform.position.y, playerItem.position.z);
        //transform.LookAt(direction);
        rb.AddForce(transform.forward * speed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (currentEnemy == collision.transform)
        {
            animator.SetBool("IsInteracting", true);
            _startedInteracting = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        animator.SetBool("IsInteracting", false);
        _startedInteracting = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, PoliceRadius);
    }

    public Rigidbody GetRigidbody() => rb;
}
