﻿using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using static PlayerInput;

public class PlayerController : OutOfThisWorldListener, IPlayerActions, IPassanger
{
    enum PlayerTool
    {
        Default,
        Wrench,
        Baton,
        Bridge
    }

    const float PlayerRadius = 1.1f;

    [SerializeField] Animator animator;
    [SerializeField] Transform interactionSphere;
    [SerializeField] Rigidbody rigidBody;
    [SerializeField] [Range(1, 500)] float speed;
    [SerializeField] [Range(1, 5)] float jumpSpeed;
    [SerializeField] AudioSource audioData;

    public Rigidbody RigidBody => rigidBody;

    PlayerInput _playerInput;
    Vector2 _movement;
    float _jump;
    AInteractableItem _currentItem;
    PlayerTool _currentTool;
    int _touchyFriends;
    float _defaultRbDrag;
    private CarController _lastCarOn;

    private void Awake()
    {
        _defaultRbDrag = rigidBody.drag;
    }

    public void OnEnable()
    {
        if (_playerInput == null)
        {
            _playerInput = new PlayerInput();
            _playerInput.Player.SetCallbacks(this);
        }
        _playerInput.Player.Enable();
    }

    public void OnDisable()
    {
        _playerInput.Player.Disable();
    }

    private void OnCollisionEnter(Collision coll)
    {
        _touchyFriends += 1;

        if (_touchyFriends == 1)
        {
            rigidBody.drag = 0;
        }

        if (coll.rigidbody)
        {
            var c = coll.rigidbody.GetComponent<CarController>();
            if (c)
            {
                _lastCarOn = c;
            }
        }
    }

    private void OnCollisionExit()
    {
        _touchyFriends -= 1;

        if (_touchyFriends == 0)
        {
            rigidBody.drag = _defaultRbDrag;
        }
    }

    public void OnFire(InputAction.CallbackContext context)
    {
        if (context.ReadValue<float>() == 1f)
        {
            Interact();
        }
        else
        {
            animator.SetBool("IsInteracting", false);
        }
    }

    public void OnLook(InputAction.CallbackContext context)
    {
        //buh
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        _movement = context.ReadValue<Vector2>();
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        if (_touchyFriends > 0)
        {
            Debug.Log("JUMP");
            _jump = context.ReadValue<float>();
            Jump();
        }
    }

    public void OnRepair(InputAction.CallbackContext context)
    {
        //buh
    }


    public void OnRestart(InputAction.CallbackContext context)
    {
        App.RestartEveything();
    }

    void FixedUpdate()
    {
        Move();

        //todo: maybe change transform.position to a n empty in front of the player
        var interactableObjects = Physics.OverlapSphere(transform.position, PlayerRadius);

        foreach (var c in interactableObjects)
        {
            if (c.TryGetComponent(out AInteractableItem item) && c.gameObject != gameObject)
            {
                if (item is RepairableItem)
                {
                    _currentTool = PlayerTool.Wrench;
                    _currentItem = item;
                    return;
                }
                else if (item is BridgeableItem)
                {
                    _currentTool = PlayerTool.Wrench;
                    _currentItem = item;
                    return;
                }
                else if (item is DebugableItem)
                {
                    _currentTool = PlayerTool.Default;
                    _currentItem = item;
                    return;
                }
            }
            else if (c.gameObject != gameObject)
            {
                _currentTool = PlayerTool.Default;
                _currentItem = null;
            }
        }
    }

    void Move()
    {
        var movement = new Vector3(_movement.x, 0f, _movement.y).normalized;
        if (_movement == Vector2.zero)
        {
            animator.SetBool("IsWalking", false);
        }
        else
        {
            animator.SetBool("IsWalking", true);
            rigidBody.AddForce(movement * speed);
        }
    }

    void Jump()
    {
        if (_jump == 1f)
        {
            audioData.Play(0);
            rigidBody.AddForce(new Vector3(0f, _jump * jumpSpeed, 0f), ForceMode.Impulse);
            animator.Play("metarig_JumpAnim");
        }
    }

    void Interact()
    {
        if (_currentTool == PlayerTool.Default) return;


        if (_currentItem != null)
        {
            switch (_currentItem)
            {
                case RepairableItem r:
                    r.Interact(animator);
                    break;
                case BridgeableItem b:
                    b.Interact(animator);
                    break;
                case DebugableItem d:
                    d.Interact(animator);
                    break;
                default:
                    Debug.LogError("Whoops, no Interactable item?!");
                    break;
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, PlayerRadius);
    }

    public Rigidbody GetRigidbody() => rigidBody;

    public void Respawn()
    {
        var car = _lastCarOn;

        if (!_lastCarOn)
        {
            car = FindObjectsOfType<CarController>().First();
        }

        if (!car)
        {
            App.RestartEveything();
        }

        car.SpawnPassenger(this, Random.Range(0, 4));
    }
}
