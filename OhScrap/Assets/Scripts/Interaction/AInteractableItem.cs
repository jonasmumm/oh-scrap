﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AInteractableItem : MonoBehaviour
{
    public virtual void Interact(Animator animator)
    {
        Debug.Log("INTERACTING");
        animator.SetBool("IsInteracting", true);
        //todo show something
    }
}
