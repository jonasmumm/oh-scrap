﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DebugableItem : AInteractableItem
{
    [SerializeField] UnityEvent eventd;

   public override void Interact(Animator animator)
    {
        eventd.Invoke();
    }
}
