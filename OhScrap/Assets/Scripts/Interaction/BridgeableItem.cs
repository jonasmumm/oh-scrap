﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeableItem : AInteractableItem
{
    const float BridgeRadius = 4f;

    [SerializeField] GameObject bridgePrefab;
    [SerializeField] CarController carController;

    BridgeableItem _bridgeBuddy;  //not null if connected to someone
    private GameObject _bridgeGameObject;

    public CarController CarController => carController;

    public override void Interact(Animator animator)
    {
        if (_bridgeBuddy == null)
        {
            base.Interact(animator);

            var closestBridgePoint = GetClosestBridgePoint(BuildABridge());
            if (closestBridgePoint == null) return;

            var direction = closestBridgePoint.transform.position - transform.position;

            _bridgeGameObject = Instantiate(bridgePrefab, transform);
            _bridgeGameObject.transform.LookAt(closestBridgePoint.transform.position);
            _bridgeGameObject.transform.localScale += new Vector3(1f, 1f, direction.magnitude);

            carController.ConnectOther(this, closestBridgePoint);
            closestBridgePoint._bridgeBuddy = this;
            this._bridgeBuddy = closestBridgePoint;
        }
    }

    BridgeableItem[] BuildABridge()
    {
        var interactableObjects = Physics.OverlapSphere(transform.position, BridgeRadius);
        var result = new BridgeableItem[interactableObjects.Length];

        for (var i = 0; i < result.Length; i++)
        {
            var c = interactableObjects[i];

            if (c.TryGetComponent(out BridgeableItem item))
            {
                if (item.carController != carController)
                {
                    result[i] = item;
                }
            }
        }
        return result;
    }

    public void DissolveBridge()
    {
        _bridgeBuddy = null;
        carController = null;

        if (_bridgeGameObject) Destroy(_bridgeGameObject);
    }

    // https://forum.unity.com/threads/clean-est-way-to-find-nearest-object-of-many-c.44315/ :-)
    BridgeableItem GetClosestBridgePoint(BridgeableItem[] bridgePoints)
    {
        BridgeableItem closesBridgePoint = null;

        float closestDistance = Mathf.Infinity;
        var currentPos = transform.position;

        foreach (var potentialTarget in bridgePoints)
        {
            if (potentialTarget == null || potentialTarget.gameObject == gameObject) continue;
            Vector3 directionToTarget = potentialTarget.transform.position - currentPos;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistance)
            {
                closestDistance = dSqrToTarget;
                closesBridgePoint = potentialTarget;
            }
        }

        return closesBridgePoint;
    }
}
