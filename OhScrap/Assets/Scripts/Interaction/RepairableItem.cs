﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemState
{
    NotBroken,
    Broken
}
public class RepairableItem : AInteractableItem
{
    const float ItemRepairTime = 2f;
    const int Hitpoints = 5;

    [SerializeField] ParticleSystem itemBrokenParticles;
    [SerializeField] ParticleSystem itemRepaired;
    [SerializeField] AudioSource repairSound;

    float _itemBreakTime;
    ItemState _state;
    int _timesHit;
    int _timesHitByPolice;
    bool _coroutineStarted;

    public ItemState State => _state;

    private void Awake()
    {
        _state = ItemState.NotBroken;
        _itemBreakTime = Random.Range(10, 30);
    }
    private void Start()
    {
        StartCoroutine(GetItemDamaged());
        Debug.Log(_itemBreakTime);
        _coroutineStarted = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (_state == ItemState.Broken || _timesHit != 0 || _timesHitByPolice != 0)
        {
            _coroutineStarted = false;
            StopCoroutine(GetItemDamaged());
        }
        else if(!_coroutineStarted && _state == ItemState.NotBroken)
        {
            StartCoroutine(GetItemDamaged());
        }
    }

    IEnumerator GetItemDamaged()
    {
        _coroutineStarted = true;
        yield return new WaitForSeconds(_itemBreakTime);
        Debug.Log("Item was broken after time");

        _state = ItemState.Broken;
        itemBrokenParticles.Play();
        _itemBreakTime = Random.Range(15, 25);
    }

    public override void Interact(Animator animator)
    {
        if (_state == ItemState.Broken)
        {
            base.Interact(animator);
            _timesHit++;
            repairSound.Play(0);

            if (_timesHit == Hitpoints)
            {
                _state = ItemState.NotBroken;
                itemBrokenParticles.Stop();
                itemRepaired.Play();
                _timesHit = 0;
            }
        }
    }

    public void PoliceInteraction()
    {
        if(_state == ItemState.NotBroken)
        {
            _timesHitByPolice++;

            if(_timesHitByPolice >= Hitpoints)
            {
                Debug.Log("Item was broken by police");
                _state = ItemState.Broken;
                itemBrokenParticles.Play();
                _timesHitByPolice = 0;
            }
        }
    }
}
