﻿using UnityEngine;
using UnityEngine.Events;

public class OutOfThisWorldListener : MonoBehaviour
{
    [SerializeField] UnityEvent OnFallOut;

    protected virtual void Update()
    {
        if (transform.position.z < -60 || transform.position.y < -40f)
        {
            OnFallOut.Invoke();
        }
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
}
